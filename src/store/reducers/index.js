import { LOGIN_TODO, DELETE_TOKEN, GET_TODO } from '../actions/actionTypes'

const initialState = {
  isLogin: localStorage.getItem('todo-token') ? true : false,
  todo: []
}

const todo = ( state = initialState, action ) =>{
  switch (action.type) {
    case DELETE_TOKEN:
      localStorage.removeItem('todo-token');
      return { ...state,  isLogin: action.payload}
    case LOGIN_TODO:
      return {...state, isLogin: action.payload}
    case GET_TODO:
      return { ...state, todo: action.payload }
    default:
      return state;
  }
}

export default todo;