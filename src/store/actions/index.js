import { GET_TODO } from './actionTypes';
import api from '../../config/request'


export const fetchData = (data) => {
  return {
    type: GET_TODO,
    payload: data
  }
};

export const fetchTodoData = (subPath) => {
  return (dispatch) => {
    return api.todo(subPath)
      .then(data => {
        dispatch(fetchData(data))
      })
      .catch(error => {
        throw(error);
      });
  };
};