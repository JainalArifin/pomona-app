import React, { Component } from 'react';
import api from '../../config/request';
import { Redirect } from 'react-router-dom';
import { LOGIN_TODO } from '../../store/actions/actionTypes'
import { connect } from 'react-redux';

import './login-register.css';
class LoginRegister extends Component {
  state = {
    righPanel: '',
    name: '',
    email: '',
    password: '',
    redirect: false,
    isLoad: false,
    validate: false,
    validateLogin: false
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleRegister = (event) => {
    event.preventDefault();
    const { name, email, password } = this.state

    this.setState({isLoad:true})

    api.register('register', {name:name, email: email, password:password})
    .then((respon)=>{
      // console.log("=== response ==>", respon)
      if(respon.data.error === "akun sudah di gunakan"){
        this.setState({ validate: true, isLoad: false, validateLogin: false })
      }else{
          this.setState({
            righPanel: '',
            name: '',
            email: '',
            password: '',
            isLoad: false,
            validate: false,
            validateLogin: false
          })
      }
    })
  }

  handleSignIn = (event) => {
    event.preventDefault();
    const {  email, password } = this.state
    
    this.setState({isLoad:true})

    api.login('login', {email: email, password:password})
    .then((respon)=>{
      if(respon.data.error === "akun sudah di gunakan"){
        this.setState({ validateLogin: true, isLoad: false, validate: false })
      }else{
        localStorage.setItem('todo-token', respon.data.token)
        this.props.isLogin()
        this.setState({
          righPanel: '',
          name: '',
          email: '',
          password: '',
          redirect: true,
          isLoad:false,
          validate: false,
          validateLogin: false
        })
      }
    })
  }

  render() {
    const { 
      righPanel, name, email, password, redirect, isLoad, 
      validate, validateLogin
    } = this.state;

    if(redirect){
      return <Redirect to="/" />
    }
    return (
      <div className="container mt-5  d-flex justify-content-center">
        {/* <h2>Weekly Coding Challenge #1: Sign in/up Form</h2> */}
        <div className={`container-log ${righPanel}`} id="container">


          {/* register start */}
          <div className="form-container sign-up-container">
            <form onSubmit={this.handleRegister}>
              <h1>Create Account</h1>
              {validate && <span style={{ color: 'red' }}>akun sudah di gunakan</span>}
              <input 
                type="text" placeholder="Name" className="mt-3" 
                name="name" value={name} onChange={this.handleChange}
              />
              <input 
                type="email" placeholder="Email" className="mt-3" 
                name="email" value={email} onChange={this.handleChange}
                />
              <input 
                type="password" placeholder="Password" className="mt-3" 
                name="password" value={password} onChange={this.handleChange}
                />
              <button 
                className="btn-form"
                type="submit" value="Submit" 
               > {isLoad ? 'loading...' : 'Sign Up'} </button>
            </form>
          </div>
          {/* register end */}


          {/* login start */}
          <div className="form-container sign-in-container">
            <form onSubmit={this.handleSignIn}>
              <h1>Sign in</h1>
              {validateLogin && <span style={{ color: 'red' }}>akun dan password salah</span>}
              <input 
                type="email" placeholder="Email" className="mt-3"
                name="email" value={email} onChange={this.handleChange}
                 />
              <input 
                type="password" placeholder="Password" className="mt-3"
                name="password" value={password} onChange={this.handleChange}
                 />
              {/* <a href="#">Forgot your password?</a> */}
              <button 
                className="btn-form"
                type="submit" value="Submit" 
              > {isLoad ? 'loading...' : 'Sign In'}</button>
            </form>
          </div>
          {/* login end */}


          <div className="overlay-container">
            <div className="overlay">
              <div className="overlay-panel overlay-left">
                <h1>Welcome Back!</h1>
                <p>To Do list</p>
                <button className="ghost" id="signIn"
                  onClick={() => this.setState({ 
                    righPanel: '',
                    name: '',
                    email: '',
                    password: '',
                    validate: false,
                    validateLogin: false
                  })}
                >Sign In</button>
              </div>
              <div className="overlay-panel overlay-right">
                <h1>Hello, Friend!</h1>
                <p>Klik SignUp untuk mendaftar</p>
                <button
                  className="ghost"
                  id="signUp"
                  onClick={() =>this.setState({ 
                    righPanel: 'right-panel-active',
                    name: '',
                    email: '',
                    password: '',
                    validate: false,
                    validateLogin: false
                  })}
                >Sign Up</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    isLogin: ()=>dispatch({ type: LOGIN_TODO, payload: true })
  }
}
export default connect(
  mapStateToProps, mapDispatchToProps
)(LoginRegister);