import React, { Component } from 'react';
import TodoList from '../../components/TodoList/TodoList';
import Layout from '../../components/Layout';
 
class Home extends Component {
  render() { 
    return (
      <Layout>
        <div className="container">
          <TodoList />
        </div>
      </Layout>
    );
  }
}
 
export default Home;