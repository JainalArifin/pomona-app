import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import Home from './pages/Home/Home';
import LoginRegister from './pages/LoginRegister/LoginRegister';


const PrivateRoute = ({ component, ...rest }) => {
  const isAuthed = localStorage.getItem('todo-token')
  return (
    <Route {...rest} exact
      render={(props) => (
        isAuthed ? (
          <div>
            {React.createElement(component, props)}
          </div>
        ) :
          (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: props.location }
              }}
            />
          )
      )}
    />
  )
}

const Routes = () => (
  <div>
    <Switch>
      <PrivateRoute path="/" component={Home} exact={true} />
      <Route path="/login" component={LoginRegister} />
    </Switch>
  </div>
);

export default Routes;