import axios from 'axios';
const api = "https://pomonatodo.herokuapp.com"; 

const instance = axios.create({
  baseURL: api
})

export default instance;