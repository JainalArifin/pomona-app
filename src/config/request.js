import instance from './instance'

const Get = (path, subPath) => {
  const promise = new Promise((resolve, reject) => {
    const token = localStorage.getItem('todo-token')
    instance.get(`/${path}/${subPath ? subPath : ''}`, {
      headers: { Authorization: token }
    })
    .then(({ data }) => {
      resolve(data)
    },(err) => {
      reject(err)
    })
  })

  return promise
}


const Post = (path, subPath, data) => {
  const promise = new Promise((resolve, reject) => {
    const token = localStorage.getItem('todo-token')
    instance({
      url: `/${path}/${subPath}`,
      method: 'POST',
      data: data,
      headers: { Authorization: token }
    })
    .then(({ data })=>{
      resolve(data)
    }, (err) => {
      // console.log('--- errorr ----', err.response.status)
      if(err.response.status === 500){
        resolve({ data: {error: 'akun sudah di gunakan'} })
      }
      reject(err)
    })
  })

  return promise
}


const Put = (path, subPath, data) => {
  const promise = new Promise((resolve, reject) => {
    const token = localStorage.getItem('todo-token')
    instance({
      url: `/${path}/${subPath}`,
      method: 'PUT',
      data: data,
      headers: { Authorization: token }
    })
    .then(({ data })=>{
      resolve(data)
    }, (err) => {
      reject(err)
    })
  })

  return promise
}


const Delete = (path, subPath) => {
  const promise = new Promise((resolve, reject) => {
    const token = localStorage.getItem('todo-token')
    instance.delete(`/${path}/${subPath ? subPath : ''}`, {
      headers: { Authorization: token }
    })
    .then(({ data }) => {
      resolve(data)
    },(err) => {
      reject(err)
    })
  })

  return promise
}

const register = (subPath, data) => Post('auth', subPath, data);
const login = (subPath, data) => Post('auth', subPath, data);

const todo = (subPath) => Get('todo', subPath);
const todoEdit = (subPath, data) => Put('todo', subPath, data);
const todoAdd = (subPath, data) => Post('todo', subPath, data);
const deleteTodo = (subPath) => Delete('todo', subPath)

const api = {
  register,
  login,
  todo,
  todoEdit,
  todoAdd,
  deleteTodo
};

export default api;