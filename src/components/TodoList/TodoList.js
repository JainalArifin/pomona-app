import React, { Component } from 'react';
import List from './List';
import AddTodo from './AddTodo';

import { connect } from 'react-redux';
import { fetchTodoData } from '../../store/actions'
import SearchTodo from './SearchTodo';
import FilterTodo from './FilterTodo';

// import './todolist.css';
class TodoList extends Component {
  componentDidMount(){
    // this.getTodo()
    this.props.fetchTodo('user')
  }

  render() { 
    return (
      <div>
      {/* add todo start */}
      <AddTodo />
      {/* add todo end */}
      <div className="todo_list">
        <SearchTodo />
        <ul>
          {/*TO DO ITEM START*/}
          <List 
            dataTodo={this.props.todo}
          />
          {/*TO DO ITEM END*/}
        </ul>
        <FilterTodo />
      </div>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todo: state.todo.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTodo: (subPath)=>dispatch(fetchTodoData(subPath))
  }
}
export default connect(
  mapStateToProps, mapDispatchToProps
)(TodoList);