import React, { Component } from 'react';

import { fetchTodoData } from '../../store/actions'
import { connect } from 'react-redux';

class FilterTodo extends Component {
  render() {
    return (
      <div className="text-center pb-3">
        <div className="btn-group btn-group-sm" role="group" aria-label="Basic example">
          <button 
            type="button" className="btn btn-secondary"
            onClick={()=>this.props.fetchTodo(`user/?filter=done`)}
            >Done</button>
          <button 
            type="button" className="btn btn-secondary"
            onClick={()=>this.props.fetchTodo(`user/?filter=undone`)}
            >Undone</button>
          <button 
            type="button" className="btn btn-secondary"
            onClick={()=>this.props.fetchTodo(`user/?filter=all`)}
            >All</button>
        </div>
      </div>
    );
  }
}


const mapStateToProps = () => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchTodo: (subPatch) => dispatch(fetchTodoData(subPatch))
  }
}
export default connect(
  mapStateToProps, mapDispatchToProps
)(FilterTodo);