import React, { Component } from 'react';
import api from '../../config/request'

import deleteTodo from '../../assets/images/delete.svg'
import EditTodo from './EditTodo';
import ViewDetail from './ViewDetail';

import { fetchTodoData } from '../../store/actions'
import { connect } from 'react-redux';

import './todolist.css';
class List extends Component {
  state = {
    isDone: false,
    isLoad: false,
    id: ''
  }
  handleInputChange = (id, event) => {
    this.setState({ isLoad: true, id: id })
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    api.todoEdit(id, { isDone: value })
      .then(() => {
        this.setState({ isLoad: false })
        this.props.fetTodo('user')
      })
  }

  handleDelete = (id) => {
    this.setState({ isLoad: true, id: id })

    api.deleteTodo(id)
    .then(()=>{
      this.setState({ isLoad: false })
      this.props.fetTodo('user')
    })
  }

  render() {
    const { id } = this.state
    return (
      <>
        {this.props.dataTodo ? this.props.dataTodo && this.props.dataTodo.map((list, key) => (
          <li key={key}
            className="border-bottom d-flex justify-content-between align-items-center pr-3"
          >
            {/* {console.log("=== list ok =>",typeof list.isDone)} */}
            <label >
              <div>
                <input
                  type="checkbox"
                  className="hidden_real_check"
                  name="isDone"
                  checked={list.isDone}
                  onChange={(e) => this.handleInputChange(list.id, e)}
                />
                <div className="todo_element cursor-pointer">
                  <span className="customized_ckeck">
                    <span className="checkbox">
                      <span className="internal_one" />
                      <span className="internal_two" />
                    </span>
                  </span>
                  <span className="element_title pl-3">
                    {parseInt(id) === list.id ? this.state.isLoad ? 'load...' : list.title : list.title}
                  </span>
                </div>
              </div>
            </label>
            <div className="d-flex">
              <EditTodo idList={list.id} />
              &nbsp; &nbsp;
              <ViewDetail idList={list.id} />
              &nbsp; &nbsp;
              <img 
                src={deleteTodo} 
                alt="delete" 
                className="edit-todo cursor-pointer" 
                onClick={()=>this.handleDelete(list.id)}
              />
            </div>
          </li>
        )) : (
            <div className="p-3">
              <div className="skelaton-todo mb-2" />
              <div className="skelaton-todo mb-2" />
              <div className="skelaton-todo mb-2" />
            </div>
          )}
      </>
    );
  }
}

const mapStateToProps = () => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetTodo: (subPatch) => dispatch(fetchTodoData(subPatch))
  }
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(List);