import React, { Component } from 'react';
import { fetchTodoData } from '../../store/actions'
import { connect } from 'react-redux';

class SearchTodo extends Component {
  state = {
    search: ''
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSearch = () => {
    this.props.fetchTodo(`user/?q=${this.state.search}`)
    this.setState({ search: '' })
  }

  handleViewAll = () => {
    this.props.fetchTodo(`user`)
    this.setState({ search: '' })
  }

  render() {
    return (
      <div className="text-center p-3">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="title..." aria-label="Recipient's username" aria-describedby="basic-addon2"
            name="search"
            value={this.state.search}
            onChange={this.handleChange}
          />
          <div className="input-group-append cursor-pointer" >
            {/* <span className="input-group-text" id="basic-addon2">Search</span> */}
            <span
              className="input-group-text"
              onClick={() => this.handleSearch()}
            >Search</span>
            <span
              className="input-group-text"
              onClick={() => this.handleViewAll()}
            >View All</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchTodo: (subPatch) => dispatch(fetchTodoData(subPatch))
  }
}
export default connect(
  mapStateToProps, mapDispatchToProps
)(SearchTodo);