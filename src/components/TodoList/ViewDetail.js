import React, { Component } from 'react';
import Modal from 'react-modal';
import viewDetail from '../../assets/images/view.svg'
import api from '../../config/request'


const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  }
};


class ViewDetail extends Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
      title: '',
      priority: '',
      note: '',
      validate: false
    };

    this.openModal = this.openModal.bind(this);
    // this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal(id) {
    this.setState({ modalIsOpen: true });
    api.todo(id)
      .then((response) => {
        console.log("=== datata =>", response.data)
        this.setState({
          title: response.data.title,
          priority: response.data.priority,
          note: response.data.note,
        })
      })
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  render() {
    const { title, priority, note } = this.state
    return (
      <div>
        <img src={viewDetail} alt="arrow" className="edit-todo cursor-pointer"
          onClick={() => this.openModal(this.props.idList)}
        />
        <Modal
          isOpen={this.state.modalIsOpen}
          ariaHideApp={false}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <div className="d-flex justify-content-end">
            <div 
              style={{ fontWeight: 'bold', color: 'red', cursor: 'pointer' }}
              onClick={()=>this.closeModal()}
              >x</div>
          </div>
          <div>
            <h3>Title</h3>
            <div>{title ? title : <div className="skelaton-title" />} </div>
          </div>
          <div>
            <h3>Priority</h3>
            <div>{priority ? priority : <div className="skelaton-title" />}</div>
          </div>
          <div>
            <h3>Note</h3>
            <div>{note ? note : <div className="skelaton-title" />}</div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default ViewDetail;