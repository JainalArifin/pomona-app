import React, { Component } from 'react';
import Modal from 'react-modal';
import api from '../../config/request'
import editIcon from '../../assets/images/edit.svg'

import { connect } from 'react-redux';
import { fetchTodoData } from '../../store/actions'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  }
};


class EditTodo extends Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
      title: '',
      priority: '',
      note: '',
      validate: false
    }; 

    this.openModal = this.openModal.bind(this);
    // this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal(id) {
    this.setState({ modalIsOpen: true });
    api.todo(id)
    .then((response)=>{
      console.log("=== datata =>", response.data)
      this.setState({
        title: response.data.title,
        priority: response.data.priority,
        note: response.data.note,
      })
    })
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  handleChange =(e)=> {
    this.setState({
      [e.target.name]: e.target.value
    },()=>{
      if(this.state.priority > 3){ 
        this.setState({ validate:true }) 
      }else{
        this.setState({ validate:false }) 
      }
    })
  }
  handleAddTodo = (e) => {
    e.preventDefault()
    const { title, priority, note } = this.state;
    if(priority < 4){
      api.todoEdit(this.props.idList, {title:title, priority:parseInt(priority), note:note })
      .then(()=>{
        this.props.fetchData('user')
        this.setState({ 
          modalIsOpen: false,
          title: '',
          priority: '',
          note: ''
        })
      })
    }
  }


  render() {
    const { title, priority ,note } = this.state
    return (
      <div>
          <img src={editIcon} alt="edit" className="edit-todo cursor-pointer" 
            onClick={()=>this.openModal(this.props.idList)}/>
        {/* <button onClick={this.openModal}>Add Todo</button> */}
        <Modal
          isOpen={this.state.modalIsOpen}
          // onAfterOpen={this.afterOpenModal}
          ariaHideApp={false}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <form onSubmit={this.handleAddTodo} className="mb-5 form-add-todo">
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input 
                type="text" className="form-control" 
                id="title" placeholder="eg. hari ulang tahun adik" 
                name="title"
                value={title}
                onChange={this.handleChange}
                />
            </div>
            <div className="form-group">
              <label htmlFor="priority">priority</label>
              {this.state.validate && (
                <div style={{ color: 'red' }}><i>minimum 1 max 3</i></div>
              )}
              <input 
                type="number" className="form-control" id="priority" placeholder="eg. 2" 
                name="priority"
                value={priority}
                onChange={this.handleChange}
                />
            </div>
            <div className="form-group">
              <label htmlFor="note">note</label>
              <textarea className="form-control" id="note" rows={3}
                name="note"
                value={note}
                onChange={this.handleChange}
              />
            </div>

            <div className="d-flex justify-content-between">
              <button 
                className="btn btn-danger"
                onClick={()=>this.setState({ modalIsOpen: false })}
              >Close</button> 
              &nbsp; &nbsp;
              <button 
                type="submit" 
                className="btn btn-primary"
              >Submit</button>
            </div>
          </form>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = () => {
  return {}
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (subPath)=> dispatch(fetchTodoData(subPath))
  }
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(EditTodo);