import React, { Component } from 'react';
import logOut from '../../assets/images/exit.svg' 
import jwt from 'jsonwebtoken'
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { DELETE_TOKEN } from '../../store/actions/actionTypes'


import './header.css'
class Header extends Component {
  state = {
    name: ''
  }

  getData = () => {
    let token  = localStorage.getItem('todo-token')
    let decoded = jwt.decode(token)
    this.setState({ name: decoded.name })
  }

  componentDidMount(){
    this.getData()
  }  

  render() { 
    const { name } = this.state;

    if(this.props.isLogin === false){
      return <Redirect to="/login" />
    }
    return (
      <div className="container-fluid header mb-3 d-flex justify-content-between align-items-center">
        <div>{ name }</div>
        <div className="todo-list">TodoList</div>
        <div>
          <img src={logOut} alt="log-out" className="logout" 
            onClick={()=>this.props.logOutBtn(false)}
          />
        </div>
      </div>
    );
  }
}
 
const mapStateToProps = (state) => {
  return {
    isLogin: state.isLogin
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    logOutBtn: (payload)=> dispatch({ type: DELETE_TOKEN, payload})
  }
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(Header);