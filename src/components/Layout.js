import React from 'react';
import Header from './Header/Header';


const Layout = (props) =>(
  <>
  <Header />
  {props.children}
  </>
)

export default Layout;